import airflow
from builtins import range
# from airflow.contrib.operators import KubernetesOperator
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.contrib.kubernetes.volume_mount import VolumeMount  # noqa
from airflow.contrib.kubernetes.volume import Volume  # noqa
from airflow.models import DAG
from datetime import timedelta
import datetime

from datetime import datetime

args = {
    'owner': 'airflow',
    # dates are inclusive
    'start_date': datetime.strptime('Jun 25 2018', '%b %d %Y'),
    'end_date': datetime.strptime('Oct 14 2018', '%b %d %Y')


}

dag = DAG(
    dag_id='daily-tron-dag',
    default_args=args,
    schedule_interval='0 0 * * *',
    dagrun_timeout=timedelta(minutes=120))

# each task will cause 2 k8 containers to run:
#   - the executor: dailytrondagggblockchaintogcs-63d3472d58534df1b3e1baf9f6656ff1
#   - and the actual work: daily-tron-task-raw-2d9b1252
blockchain_to_gcs = KubernetesPodOperator(namespace="default",
                                          image="425586862811.dkr.ecr.us-east-1.amazonaws.com/voyager-api:latest",
                                          arguments=[
                                              "--trace-warnings", "--max_old_space_size=1500", "tron-by-day.js"],
                                          image_pull_policy="Always",
                                          env_vars={
                                              "DATE": "{{ execution_date.strftime('%Y-%m-%d') }}",
                                              "CHAIN_HOST": "troncore-mainnet",
                                              "CHAIN_PORT": "8090",
                                              "GCS_BUCKET_NAME": "messari-blockchains-raw",
                                              "GOOGLE_APPLICATION_CREDENTIALS": "/etc/gcloud-secret/svc_account.json"
                                          },
                                          volumes=[
                                              Volume(name="gcloud-secret",
                                                     configs={"secret": {"secretName": "gcloud-secret"}}),
                                          ],
                                          volume_mounts=[
                                              VolumeMount("gcloud-secret",
                                                          mount_path="/etc/gcloud-secret",
                                                          sub_path=None,
                                                          read_only=False)
                                          ],
                                          # k8 container name running the
                                          # actual voyager image
                                          name="tron-blockchain-to-gcs-task-instance",
                                          # k8 container name running the
                                          # airflow task instance parent
                                          task_id="tron-blockchain-to-gcs-task",
                                          retries=3,
                                          get_logs=True,
                                          dag=dag,
                                          in_cluster=True,
                                          is_delete_operator_pod=True,
                                          task_concurrency=5
                                          )


gcs_to_bq = KubernetesPodOperator(namespace="default",
                                  image="messari/google-cloud-sdk:latest",
                                  arguments=["bq", "load",
                                             "--replace",
                                             "--source_format=NEWLINE_DELIMITED_JSON",
                                             "--time_partitioning_type=DAY",
                                             "--time_partitioning_field=partitionDate",
                                             "learning.troncore_mainnet${{ execution_date.strftime('%Y%m%d') }}",
                                             "gs://messari-blockchains-raw/troncore_mainnet/{{ execution_date.strftime('%Y-%m-%d') }}/blocks.json"],
                                  image_pull_policy="Always",
                                  volumes=[
                                      Volume(name="gcloud-secret",
                                             configs={"secret": {"secretName": "gcloud-secret"}}),
                                  ],
                                  volume_mounts=[
                                      VolumeMount("gcloud-secret",
                                                  mount_path="/tmp/certs",
                                                  sub_path=None,
                                                  read_only=False)
                                  ],
                                  # k8 container name running the
                                  # actual voyager image
                                  name="tron-gcs-to-bq-task-instance",
                                  # k8 container name running the
                                  # airflow task instance parent
                                  task_id="tron-gcs-to-bq-task",
                                  retries=3,
                                  get_logs=True,
                                  dag=dag,
                                  in_cluster=True,
                                  is_delete_operator_pod=True,
                                  task_concurrency=10
                                  )


blockchain_to_gcs.set_downstream(gcs_to_bq)

if __name__ == "__main__":
  dag.cli()
